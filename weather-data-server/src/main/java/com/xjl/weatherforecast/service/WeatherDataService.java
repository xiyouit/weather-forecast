package com.xjl.weatherforecast.service;


import com.xjl.weatherforecast.vo.WeatherResponse;

/**
 * 天气服务
 * @author 佳慕流年
 * Weather Data Service.
 */
public interface WeatherDataService {
	/**
	 * 根据城市ID查询天气数据
	 * 
	 * @param cityId
	 * @return
	 */
	WeatherResponse getDataByCityId(String cityId);

	/**
	 * 根据城市名称查询天气数据
	 * @return
	 */
	WeatherResponse getDataByCityName(String cityName);

	/**
	 * 根据城市ID来同步天气
	 * @param cityId
	 */
	void syncDateByCityId(String cityId);
}
