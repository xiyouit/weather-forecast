package com.xjl.weatherforecast.service.impl;

import com.xjl.weatherforecast.service.WeatherDataService;
import com.xjl.weatherforecast.service.WeatherReportService;
import com.xjl.weatherforecast.vo.Weather;
import com.xjl.weatherforecast.vo.WeatherResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WeatherReportServiceImpl implements WeatherReportService {

    @Autowired
    private WeatherDataService weatherDataService;

    @Override
    public Weather getDataByCityId(String cityId) {
        WeatherResponse weatherResponse = weatherDataService.getDataByCityId(cityId);
        return weatherResponse.getData();
    }
}
