package com.xjl.weatherforecast.vo;

import java.io.Serializable;

/**
 * Weather Response.
 * @author 佳慕流年
 */
public class WeatherResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	private Weather data;//天气数据
	private Integer status;//状态码
	private String desc;//描述
	public Weather getData() {
		return data;
	}
	public void setData(Weather data) {
		this.data = data;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
}
